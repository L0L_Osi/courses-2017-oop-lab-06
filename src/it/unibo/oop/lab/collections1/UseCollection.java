package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {
	
	private static final int LOWER_BOUND = 1000;
	private static final int UPPER_BOUND = 2000;
	private static final int N_ELEMS = 	100000;
	private static final int START_OF_LIST = 0;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
    		
    		//1
    		List<Integer> arrayList = new ArrayList<>();
    		for(int i = LOWER_BOUND; i < UPPER_BOUND ; i++) {
    			arrayList.add(i);
    		}
    		
    		//2
    		List<Integer> linkedList = new LinkedList<>(arrayList);
    		
    		//3
    		Integer tmp = arrayList.get(arrayList.size() - 1);
    		arrayList.set(arrayList.size() - 1, arrayList.get(START_OF_LIST));
    		arrayList.set(START_OF_LIST, tmp);
    		
    		//4
    		for(int t: arrayList) {
    			System.out.print(t + " ");
    		}
    		System.out.println("");
    		
    		//5
    		long timeAB, timeA, timeB;
    		List<Integer> arrayListA = new ArrayList<>(arrayList);
    		List<Integer> linkedListA = new LinkedList<>(linkedList);
    		
    		timeAB = System.nanoTime();
    		for(int i = 0; i < N_ELEMS; i++) {
    			arrayList.add(i, START_OF_LIST);
    			linkedList.add(i, START_OF_LIST);
    		}
    		timeAB = System.nanoTime() - timeAB;
    		
    		timeA = System.nanoTime();
    		for(int i = 0; i < N_ELEMS; i++) {
    			arrayListA.add(i, START_OF_LIST);
    		}
    		timeA = System.nanoTime() - timeA;
    		
    		timeB = System.nanoTime();
    		for(int i = 0; i < N_ELEMS; i++) {
    			linkedListA.add(i, START_OF_LIST);
    		}
    		timeB = System.nanoTime() - timeB;
    		
    		System.out.println("Time to add " + N_ELEMS + " in the ArrayList = " + timeA);
    		System.out.println("Time to add " + N_ELEMS + " in the LinkedList = " + timeB);
    		System.out.println("Time to add " + N_ELEMS + " in the ArrayList and the LinkedList = " + timeAB);
    		System.out.println("");
    		
    		//6
    		int position = arrayList.size() / 2;
    		
    		timeAB = System.nanoTime();
    		for(int i = 0; i < LOWER_BOUND; i++) {
    			arrayList.get(position);
    			linkedList.get(position);
    		}
    		timeAB = System.nanoTime() - timeAB;
    		
    		timeA = System.nanoTime();
    		for(int i = 0; i < LOWER_BOUND; i++) {
    			arrayList.get(position);
    		}
    		timeA = System.nanoTime() - timeA;
    		
    		timeB = System.nanoTime();
    		for(int i = 0; i < LOWER_BOUND; i++) {
    			linkedList.get(position);
    		}
    		timeB = System.nanoTime() - timeB;
    		
    		System.out.println("Time to get " + LOWER_BOUND + " elements from " + position + " in the ArrayList = " + timeA);
    		System.out.println("Time to get " + LOWER_BOUND + " elements from " + position + " in the LinkedList = " + timeB);
    		System.out.println("Time to get " + LOWER_BOUND + " elements from " + position + " in the ArrayList and the LinkedList = " + timeAB);
    		System.out.println("");
    		
    		//7
    		Map<String, Long> world = new HashMap<>();
    		world.put("Africa", 1110635000L);
    		world.put("Americas", 972005000L);
    		world.put("Antarctica", 0L);
    		world.put("Asia", 4298723000L);
    		world.put("Europe", 1110635000L);
    		world.put("Oceania", 38304000L);
    		
    		//8
    		long population = 0;
    		for (final long i: world.values()) {
    			population += i;
    		}
    		System.out.println("World population = " + population);
    	}
}
